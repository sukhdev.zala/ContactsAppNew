﻿using ContactsAppWeb.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContactsAppWeb.Core
{
    public class Utilities
    {
        ContactsAppContext context;

        public Utilities()
        {
            context = new ContactsAppContext();
        }

        public IEnumerable<ContactType> GetContactTypes()
        {
            var data = context.ContactTypes;
            return data.ToList();
        }

        public IEnumerable<Relationship> GetRelations()
        {
            var data = context.Relationships;
            return data.ToList();
        }

        public IEnumerable<MemberOf> GetMembersOf()
        {
            var data = context.MemberOfs;
            return data.ToList();
        }

        public Dictionary<string, string> GetStates()
        {
            Dictionary<string, string> states = new Dictionary<string, string>
            {
                {"AL","Alabama"},
                {"AK","Alaska"},
                {"AZ","Arizona"},
                {"AR","Arkansas"},
                {"CA","California"},
                {"CO","Colorado"},
                {"CT","Connecticut"},
                {"DE","Delaware"},
                {"FL","Florida"},
                {"GA","Georgia"},
                {"HI","Hawaii"},
                {"ID","Idaho"},
                {"IL","Illinois"},
                {"IN","Indiana"},
                {"IA","Iowa"},
                {"KS","Kansas"},
                {"KY","Kentucky"},
                {"LA","Louisiana"},
                {"ME","Maine"},
                {"MD","Maryland"},
                {"MA","Massachusetts"},
                {"MI","Michigan"},
                {"MN","Minnesota"},
                {"MS","Mississippi"},
                {"MO","Missouri"},
                {"MT","Montana"},
                {"NE","Nebraska"},
                {"NV","Nevada"},
                {"NH","New Hampshire"},
                {"NJ","New Jersey"},
                {"NM","New Mexico"},
                {"NY","New York"},
                {"NC","North Carolina"},
                {"ND","North Dakota"},
                {"OH","Ohio"},
                {"OK","Oklahoma"},
                {"OR","Oregon"},
                {"PA","Pennsylvania"},
                {"RI","Rhode Island"},
                {"SC","South Carolina"},
                {"SD","South Dakota"},
                {"TN","Tennessee"},
                {"TX","Texas"},
                {"UT","Utah"},
                {"VT","Vermont"},
                {"VA","Virginia"},
                {"WA","Washington"},
                {"WV","West Virginia"},
                {"WI","Wisconsin"},
                {"WY","Wyoming"}
            };
            return states;
        }

        public IEnumerable<PowerOfAttorneyType> GetPowerOfAttorneyTypes()
        {
            var data = context.PowerOfAttorneyTypes;
            return data.ToList();
        }

        public IEnumerable<TaxType> GetTaxTypes()
        {
            var data = context.TaxTypes;
            return data.ToList();
        }

        public IEnumerable<Jurisdiction> GetJurisdictions()
        {
            var data = context.Jurisdictions;
            return data.ToList();
        }

        public IEnumerable<MemberOf> GetAllMembersList()
        {
            var data = context.MemberOfs;
            return data.ToList();
        }
    }
}