using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ContactsAppWeb.Core.Models.Mapping
{
    public class TaxTypeMap : EntityTypeConfiguration<TaxType>
    {
        public TaxTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.TaxType1)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TaxType", "contact");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.TaxType1).HasColumnName("TaxType");
        }
    }
}
