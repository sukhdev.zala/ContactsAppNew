using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ContactsAppWeb.Core.Models.Mapping
{
    public class PowerOfAttorneyTypeMap : EntityTypeConfiguration<PowerOfAttorneyType>
    {
        public PowerOfAttorneyTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.PowerOfAttorneyType1)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("PowerOfAttorneyType", "contact");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.PowerOfAttorneyType1).HasColumnName("PowerOfAttorneyType");
        }
    }
}
