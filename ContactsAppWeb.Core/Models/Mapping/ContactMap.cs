using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ContactsAppWeb.Core.Models.Mapping
{
    public class ContactMap : EntityTypeConfiguration<Contact>
    {
        public ContactMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.OfficeLocation)
                .HasMaxLength(100);

            this.Property(t => t.BusinessUnitsAudited)
                .IsRequired()
                .HasMaxLength(75);

            this.Property(t => t.BerkshireProjects)
                .IsRequired();

            this.Property(t => t.BusinessPhone)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.SecondaryBusinessPhone)
                .HasMaxLength(20);

            this.Property(t => t.BusinessEmailAddress)
                .IsRequired()
                .HasMaxLength(75);

            this.Property(t => t.MobileNumber)
                .HasMaxLength(20);

            this.Property(t => t.FaxNumber)
                .HasMaxLength(20);

            this.Property(t => t.JobTitle)
                .HasMaxLength(50);

            this.Property(t => t.ManagerName)
                .HasMaxLength(75);

            this.Property(t => t.AssitantName)
                .HasMaxLength(75);

            this.Property(t => t.AssistantPhone)
                .HasMaxLength(20);

            this.Property(t => t.Specialty)
                .HasMaxLength(50);

            this.Property(t => t.City)
                .HasMaxLength(50);

            this.Property(t => t.StateProvince)
                .HasMaxLength(50);

            this.Property(t => t.Country)
                .HasMaxLength(50);

            this.Property(t => t.CompanyBio)
                .HasMaxLength(100);

            this.Property(t => t.TwitterHandle)
                .HasMaxLength(100);

            this.Property(t => t.LinkedIn)
                .HasMaxLength(320);

            this.Property(t => t.Certifications)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Contact", "contact");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.GUID).HasColumnName("GUID");
            this.Property(t => t.ContactTypeID).HasColumnName("ContactTypeID");
            this.Property(t => t.RelationshipID).HasColumnName("RelationshipID");
            this.Property(t => t.OfficeLocation).HasColumnName("OfficeLocation");
            this.Property(t => t.BusinessUnitsAudited).HasColumnName("BusinessUnitsAudited");
            this.Property(t => t.BerkshireProjects).HasColumnName("BerkshireProjects");
            this.Property(t => t.BusinessPhone).HasColumnName("BusinessPhone");
            this.Property(t => t.SecondaryBusinessPhone).HasColumnName("SecondaryBusinessPhone");
            this.Property(t => t.BusinessEmailAddress).HasColumnName("BusinessEmailAddress");
            this.Property(t => t.MobileNumber).HasColumnName("MobileNumber");
            this.Property(t => t.ActiveContact).HasColumnName("ActiveContact");
            this.Property(t => t.FaxNumber).HasColumnName("FaxNumber");
            this.Property(t => t.MemberOfID).HasColumnName("MemberOfID");
            this.Property(t => t.JobTitle).HasColumnName("JobTitle");
            this.Property(t => t.ManagerName).HasColumnName("ManagerName");
            this.Property(t => t.AssitantName).HasColumnName("AssitantName");
            this.Property(t => t.AssistantPhone).HasColumnName("AssistantPhone");
            this.Property(t => t.Specialty).HasColumnName("Specialty");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.StateProvince).HasColumnName("StateProvince");
            this.Property(t => t.ZipPostalCode).HasColumnName("ZipPostalCode");
            this.Property(t => t.Country).HasColumnName("Country");
            this.Property(t => t.PowerOfAttorney).HasColumnName("PowerOfAttorney");
            this.Property(t => t.PowerOfAttorneyTypeID).HasColumnName("PowerOfAttorneyTypeID");
            this.Property(t => t.TaxTypeID).HasColumnName("TaxTypeID");
            this.Property(t => t.JurisdictionID).HasColumnName("JurisdictionID");
            this.Property(t => t.Form8821Authority).HasColumnName("Form8821Authority");
            this.Property(t => t.Birthday).HasColumnName("Birthday");
            this.Property(t => t.WorkAnniversary).HasColumnName("WorkAnniversary");
            this.Property(t => t.CompanyBio).HasColumnName("CompanyBio");
            this.Property(t => t.TwitterHandle).HasColumnName("TwitterHandle");
            this.Property(t => t.Strengths).HasColumnName("Strengths");
            this.Property(t => t.LinkedIn).HasColumnName("LinkedIn");
            this.Property(t => t.Certifications).HasColumnName("Certifications");
            this.Property(t => t.UserGuid).HasColumnName("UserGuid");

            // Relationships
            this.HasRequired(t => t.ContactType)
                .WithMany(t => t.Contacts)
                .HasForeignKey(d => d.ContactTypeID);
            this.HasOptional(t => t.Jurisdiction)
                .WithMany(t => t.Contacts)
                .HasForeignKey(d => d.JurisdictionID);
            this.HasOptional(t => t.MemberOf)
                .WithMany(t => t.Contacts)
                .HasForeignKey(d => d.MemberOfID);
            this.HasOptional(t => t.PowerOfAttorneyType)
                .WithMany(t => t.Contacts)
                .HasForeignKey(d => d.PowerOfAttorneyTypeID);
            this.HasRequired(t => t.Relationship)
                .WithMany(t => t.Contacts)
                .HasForeignKey(d => d.RelationshipID);
            this.HasOptional(t => t.TaxType)
                .WithMany(t => t.Contacts)
                .HasForeignKey(d => d.TaxTypeID);

        }
    }
}
