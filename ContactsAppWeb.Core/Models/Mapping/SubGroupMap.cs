using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ContactsAppWeb.Core.Models.Mapping
{
    public class SubGroupMap : EntityTypeConfiguration<SubGroup>
    {
        public SubGroupMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.SubGroupName)
                .IsRequired()
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("SubGroup", "contact");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.SubGroupName).HasColumnName("SubGroupName");
            this.Property(t => t.GroupID).HasColumnName("GroupID");

            // Relationships
            this.HasRequired(t => t.Group)
                .WithMany(t => t.SubGroups)
                .HasForeignKey(d => d.GroupID);

        }
    }
}
