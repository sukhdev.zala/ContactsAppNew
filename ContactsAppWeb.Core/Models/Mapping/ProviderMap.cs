using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ContactsAppWeb.Core.Models.Mapping
{
    public class ProviderMap : EntityTypeConfiguration<Provider>
    {
        public ProviderMap()
        {
            // Primary Key
            this.HasKey(t => t.ProviderID);

            // Properties
            this.Property(t => t.Provider1)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Provider", "common");
            this.Property(t => t.ProviderID).HasColumnName("ProviderID");
            this.Property(t => t.Provider1).HasColumnName("Provider");
        }
    }
}
