using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ContactsAppWeb.Core.Models.Mapping
{
    public class BUMap : EntityTypeConfiguration<BU>
    {
        public BUMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.BU1)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Code)
                .IsRequired()
                .HasMaxLength(2);

            // Table & Column Mappings
            this.ToTable("BU", "common");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.IndustryID).HasColumnName("IndustryID");
            this.Property(t => t.BU1).HasColumnName("BU");
            this.Property(t => t.Code).HasColumnName("Code");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreateDate).HasColumnName("CreateDate");
            this.Property(t => t.CreateTime).HasColumnName("CreateTime");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifyDate).HasColumnName("ModifyDate");
            this.Property(t => t.ModifyTime).HasColumnName("ModifyTime");

            // Relationships
            this.HasRequired(t => t.Industry)
                .WithMany(t => t.BUs)
                .HasForeignKey(d => d.IndustryID);

        }
    }
}
