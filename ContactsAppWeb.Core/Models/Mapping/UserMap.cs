using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ContactsAppWeb.Core.Models.Mapping
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Login)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(25);

            this.Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.PreferredName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Email)
                .HasMaxLength(75);

            this.Property(t => t.Image)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("User", "common");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.GUID).HasColumnName("GUID");
            this.Property(t => t.Login).HasColumnName("Login");
            this.Property(t => t.UserTypeID).HasColumnName("UserTypeID");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.PreferredName).HasColumnName("PreferredName");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.BUID).HasColumnName("BUID");
            this.Property(t => t.EntityID).HasColumnName("EntityID");
            this.Property(t => t.ProviderID).HasColumnName("ProviderID");
            this.Property(t => t.Image).HasColumnName("Image");

            // Relationships
            this.HasOptional(t => t.BU)
                .WithMany(t => t.Users)
                .HasForeignKey(d => d.BUID);
            this.HasOptional(t => t.Entity)
                .WithMany(t => t.Users)
                .HasForeignKey(d => d.EntityID);
            this.HasOptional(t => t.Provider)
                .WithMany(t => t.Users)
                .HasForeignKey(d => d.ProviderID);
            this.HasRequired(t => t.UserType)
                .WithMany(t => t.Users)
                .HasForeignKey(d => d.UserTypeID);

        }
    }
}
