using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ContactsAppWeb.Core.Models.Mapping
{
    public class GroupMemberMap : EntityTypeConfiguration<GroupMember>
    {
        public GroupMemberMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(25);

            this.Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Title)
                .HasMaxLength(50);

            this.Property(t => t.BusinessPhone)
                .HasMaxLength(20);

            this.Property(t => t.BusinessEmailAddress)
                .HasMaxLength(75);

            // Table & Column Mappings
            this.ToTable("GroupMember", "contact");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.GroupID).HasColumnName("GroupID");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.BUID).HasColumnName("BUID");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.BusinessPhone).HasColumnName("BusinessPhone");
            this.Property(t => t.BusinessEmailAddress).HasColumnName("BusinessEmailAddress");
            this.Property(t => t.Strengths).HasColumnName("Strengths");
            this.Property(t => t.UserGuid).HasColumnName("UserGuid");
            this.Property(t => t.Approve).HasColumnName("Approve");

            // Relationships
            this.HasRequired(t => t.Group)
                .WithMany(t => t.GroupMembers)
                .HasForeignKey(d => d.GroupID);

        }
    }
}
