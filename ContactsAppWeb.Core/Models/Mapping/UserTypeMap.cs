using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ContactsAppWeb.Core.Models.Mapping
{
    public class UserTypeMap : EntityTypeConfiguration<UserType>
    {
        public UserTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.UserType1)
                .IsRequired()
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("UserType", "common");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.UserType1).HasColumnName("UserType");
            this.Property(t => t.DisplayOrder).HasColumnName("DisplayOrder");
        }
    }
}
