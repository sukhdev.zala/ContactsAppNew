using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ContactsAppWeb.Core.Models.Mapping
{
    public class MemberOfMap : EntityTypeConfiguration<MemberOf>
    {
        public MemberOfMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.MemberOf1)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("MemberOf", "contact");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.MemberOf1).HasColumnName("MemberOf");
        }
    }
}
