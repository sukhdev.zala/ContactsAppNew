using System;
using System.Collections.Generic;

namespace ContactsAppWeb.Core.Models
{
    public partial class PowerOfAttorneyType
    {
        public PowerOfAttorneyType()
        {
            this.Contacts = new List<Contact>();
        }

        public int ID { get; set; }
        public string PowerOfAttorneyType1 { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
    }
}
