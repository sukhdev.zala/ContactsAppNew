using System;
using System.Collections.Generic;

namespace ContactsAppWeb.Core.Models
{
    public partial class MemberOf
    {
        public MemberOf()
        {
            this.Contacts = new List<Contact>();
        }

        public int ID { get; set; }
        public string MemberOf1 { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
    }
}
