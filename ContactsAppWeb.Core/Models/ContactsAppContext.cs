using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using ContactsAppWeb.Core.Models.Mapping;

namespace ContactsAppWeb.Core.Models
{
    public partial class ContactsAppContext : DbContext
    {
        static ContactsAppContext()
        {
            Database.SetInitializer<ContactsAppContext>(null);
        }

        public ContactsAppContext()
            : base("Name=ContactsAppContext")
        {
        }

        public DbSet<BU> BUs { get; set; }
        public DbSet<Entity> Entities { get; set; }
        public DbSet<Industry> Industries { get; set; }
        public DbSet<Provider> Providers { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserType> UserTypes { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<ContactType> ContactTypes { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<GroupMember> GroupMembers { get; set; }
        public DbSet<Jurisdiction> Jurisdictions { get; set; }
        public DbSet<MemberOf> MemberOfs { get; set; }
        public DbSet<PowerOfAttorneyType> PowerOfAttorneyTypes { get; set; }
        public DbSet<Relationship> Relationships { get; set; }
        public DbSet<SubGroup> SubGroups { get; set; }
        public DbSet<SubGroupMember> SubGroupMembers { get; set; }
        public DbSet<TaxType> TaxTypes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new BUMap());
            modelBuilder.Configurations.Add(new EntityMap());
            modelBuilder.Configurations.Add(new IndustryMap());
            modelBuilder.Configurations.Add(new ProviderMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new UserTypeMap());
            modelBuilder.Configurations.Add(new ContactMap());
            modelBuilder.Configurations.Add(new ContactTypeMap());
            modelBuilder.Configurations.Add(new GroupMap());
            modelBuilder.Configurations.Add(new GroupMemberMap());
            modelBuilder.Configurations.Add(new JurisdictionMap());
            modelBuilder.Configurations.Add(new MemberOfMap());
            modelBuilder.Configurations.Add(new PowerOfAttorneyTypeMap());
            modelBuilder.Configurations.Add(new RelationshipMap());
            modelBuilder.Configurations.Add(new SubGroupMap());
            modelBuilder.Configurations.Add(new SubGroupMemberMap());
            modelBuilder.Configurations.Add(new TaxTypeMap());
        }
    }
}
