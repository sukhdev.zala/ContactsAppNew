using System;
using System.Collections.Generic;

namespace ContactsAppWeb.Core.Models
{
    public partial class Jurisdiction
    {
        public Jurisdiction()
        {
            this.Contacts = new List<Contact>();
        }

        public int ID { get; set; }
        public string Jurisdiction1 { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
    }
}
