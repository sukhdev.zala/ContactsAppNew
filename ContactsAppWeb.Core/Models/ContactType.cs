using System;
using System.Collections.Generic;

namespace ContactsAppWeb.Core.Models
{
    public partial class ContactType
    {
        public ContactType()
        {
            this.Contacts = new List<Contact>();
        }

        public int ID { get; set; }
        public string ContactType1 { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
    }
}
