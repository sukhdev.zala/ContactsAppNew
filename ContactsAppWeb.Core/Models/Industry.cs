using System;
using System.Collections.Generic;

namespace ContactsAppWeb.Core.Models
{
    public partial class Industry
    {
        public Industry()
        {
            this.BUs = new List<BU>();
        }

        public int ID { get; set; }
        public string Industry1 { get; set; }
        public bool Active { get; set; }
        public System.Guid CreatedBy { get; set; }
        public System.DateTime CreateDate { get; set; }
        public System.TimeSpan CreateTime { get; set; }
        public Nullable<System.Guid> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifyDate { get; set; }
        public Nullable<System.TimeSpan> ModifyTime { get; set; }
        public virtual ICollection<BU> BUs { get; set; }
    }
}
