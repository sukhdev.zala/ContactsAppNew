using System;
using System.Collections.Generic;

namespace ContactsAppWeb.Core.Models
{
    public partial class BU
    {
        public BU()
        {
            this.Entities = new List<Entity>();
            this.Users = new List<User>();
        }

        public int ID { get; set; }
        public int IndustryID { get; set; }
        public string BU1 { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }
        public System.Guid CreatedBy { get; set; }
        public System.DateTime CreateDate { get; set; }
        public System.TimeSpan CreateTime { get; set; }
        public Nullable<System.Guid> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifyDate { get; set; }
        public Nullable<System.TimeSpan> ModifyTime { get; set; }
        public virtual Industry Industry { get; set; }
        public virtual ICollection<Entity> Entities { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
