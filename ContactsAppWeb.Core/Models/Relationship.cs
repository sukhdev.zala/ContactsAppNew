using System;
using System.Collections.Generic;

namespace ContactsAppWeb.Core.Models
{
    public partial class Relationship
    {
        public Relationship()
        {
            this.Contacts = new List<Contact>();
        }

        public int ID { get; set; }
        public string Relationship1 { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
    }
}
