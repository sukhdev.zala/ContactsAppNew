using System;
using System.Collections.Generic;

namespace ContactsAppWeb.Core.Models
{
    public partial class TaxType
    {
        public TaxType()
        {
            this.Contacts = new List<Contact>();
        }

        public int ID { get; set; }
        public string TaxType1 { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
    }
}
