using System;
using System.Collections.Generic;

namespace ContactsAppWeb.Core.Models
{
    public partial class Provider
    {
        public Provider()
        {
            this.Users = new List<User>();
        }

        public int ProviderID { get; set; }
        public string Provider1 { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
