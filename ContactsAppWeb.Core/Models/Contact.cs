using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContactsAppWeb.Core.Models
{
    public partial class Contact
    {
        public int ID { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public System.Guid GUID { get; set; }
        public int ContactTypeID { get; set; }
        public int RelationshipID { get; set; }
        public string OfficeLocation { get; set; }
        public string BusinessUnitsAudited { get; set; }
        public string BerkshireProjects { get; set; }
        public string BusinessPhone { get; set; }
        public string SecondaryBusinessPhone { get; set; }
        public string BusinessEmailAddress { get; set; }
        public string MobileNumber { get; set; }
        public bool ActiveContact { get; set; }
        public string FaxNumber { get; set; }
        public Nullable<int> MemberOfID { get; set; }
        public string JobTitle { get; set; }
        public string ManagerName { get; set; }
        public string AssitantName { get; set; }
        public string AssistantPhone { get; set; }
        public string Specialty { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public Nullable<int> ZipPostalCode { get; set; }
        public string Country { get; set; }
        public Nullable<bool> PowerOfAttorney { get; set; }
        public Nullable<int> PowerOfAttorneyTypeID { get; set; }
        public Nullable<int> TaxTypeID { get; set; }
        public Nullable<int> JurisdictionID { get; set; }
        public Nullable<bool> Form8821Authority { get; set; }
        public Nullable<System.DateTime> Birthday { get; set; }
        public Nullable<System.DateTime> WorkAnniversary { get; set; }
        public string CompanyBio { get; set; }
        public string TwitterHandle { get; set; }
        public string Strengths { get; set; }
        public string LinkedIn { get; set; }
        public string Certifications { get; set; }
        public Nullable<System.Guid> UserGuid { get; set; }
        public virtual ContactType ContactType { get; set; }
        public virtual Jurisdiction Jurisdiction { get; set; }
        public virtual MemberOf MemberOf { get; set; }
        public virtual PowerOfAttorneyType PowerOfAttorneyType { get; set; }
        public virtual Relationship Relationship { get; set; }
        public virtual TaxType TaxType { get; set; }
    }
}
