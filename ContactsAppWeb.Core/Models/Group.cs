using System;
using System.Collections.Generic;

namespace ContactsAppWeb.Core.Models
{
    public partial class Group
    {
        public Group()
        {
            this.SubGroups = new List<SubGroup>();
            this.GroupMembers = new List<GroupMember>();
        }

        public int ID { get; set; }
        public string GroupName { get; set; }
        public virtual ICollection<SubGroup> SubGroups { get; set; }
        public virtual ICollection<GroupMember> GroupMembers { get; set; }
    }
}
