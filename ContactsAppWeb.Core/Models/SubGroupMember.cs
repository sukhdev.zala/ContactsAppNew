using System;
using System.Collections.Generic;

namespace ContactsAppWeb.Core.Models
{
    public partial class SubGroupMember
    {
        public int ID { get; set; }
        public int SubGroupID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Nullable<int> BUID { get; set; }
        public string Title { get; set; }
        public string BusinessPhone { get; set; }
        public string BusinessEmailAddress { get; set; }
        public string Strengths { get; set; }
        public System.Guid UserGuid { get; set; }
        public bool Approve { get; set; }
        public virtual SubGroup SubGroup { get; set; }
    }
}
