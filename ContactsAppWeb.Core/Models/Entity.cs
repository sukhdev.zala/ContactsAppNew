using System;
using System.Collections.Generic;

namespace ContactsAppWeb.Core.Models
{
    public partial class Entity
    {
        public Entity()
        {
            this.Users = new List<User>();
        }

        public int ID { get; set; }
        public int BUID { get; set; }
        public string Entity1 { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }
        public System.Guid CreatedBy { get; set; }
        public System.DateTime CreateDate { get; set; }
        public System.TimeSpan CreateTime { get; set; }
        public Nullable<System.Guid> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifyDate { get; set; }
        public Nullable<System.TimeSpan> ModifyTime { get; set; }
        public virtual BU BU { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
