using System;
using System.Collections.Generic;

namespace ContactsAppWeb.Core.Models
{
    public partial class SubGroup
    {
        public SubGroup()
        {
            this.SubGroupMembers = new List<SubGroupMember>();
        }

        public int ID { get; set; }
        public string SubGroupName { get; set; }
        public int GroupID { get; set; }
        public virtual Group Group { get; set; }
        public virtual ICollection<SubGroupMember> SubGroupMembers { get; set; }
    }
}
