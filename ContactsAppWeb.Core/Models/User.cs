using System;
using System.Collections.Generic;

namespace ContactsAppWeb.Core.Models
{
    public partial class User
    {
        public int ID { get; set; }
        public System.Guid GUID { get; set; }
        public string Login { get; set; }
        public int UserTypeID { get; set; }
        public bool Active { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PreferredName { get; set; }
        public string Email { get; set; }
        public Nullable<int> BUID { get; set; }
        public Nullable<int> EntityID { get; set; }
        public Nullable<int> ProviderID { get; set; }
        public string Image { get; set; }
        public virtual BU BU { get; set; }
        public virtual Entity Entity { get; set; }
        public virtual Provider Provider { get; set; }
        public virtual UserType UserType { get; set; }
    }
}
