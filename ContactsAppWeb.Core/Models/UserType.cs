using System;
using System.Collections.Generic;

namespace ContactsAppWeb.Core.Models
{
    public partial class UserType
    {
        public UserType()
        {
            this.Users = new List<User>();
        }

        public int ID { get; set; }
        public string UserType1 { get; set; }
        public int DisplayOrder { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
