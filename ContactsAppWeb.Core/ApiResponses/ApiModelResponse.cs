﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactsAppWeb.Core.ApiResponses
{
    public class ApiModelResponse
    {
        public ApiModelResponse()
        {
            Errors = new List<ModelError>();
        }
        public bool IsModelError { get; set; }
        public bool Status { get; set; }
        public string Message { get; set; }
        public IEnumerable<ModelError> Errors { get; set; }
    }

    public class ModelError
    {
        public string Key { get; set; }
        public string Error { get; set; }
    }
}
