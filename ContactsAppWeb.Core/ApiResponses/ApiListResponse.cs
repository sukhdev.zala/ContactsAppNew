﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactsAppWeb.Core.ApiResponses
{
    public class ApiListResponse<T> where T : class
    {
        public ApiListResponse()
        {
            Data = new List<T>();
        }
        public int Status { get; set; }
        public string Message { get; set; }
        public IEnumerable<T> Data { get; set; }
    }
}