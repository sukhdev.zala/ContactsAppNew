﻿'use strict';
angular.module('corsApp')
.controller('coreController', ['$scope', 'coreServices', function ($scope, svc) {

    var hoverwidth = angular.element(window).width();

    $scope.Guid = "47e90b39-2d82-4fd3-9365-6521176e9171";
    $scope.UserGuid = "47e90b39-2d82-4fd3-9365-6521176e9171";
    $scope.PageNumber = 1;
    $scope.SortField = "FirstName";

    $scope.setHeight = function () {
        var wwidth = angular.element(window).width();
        if (wwidth > 768) {
            angular.element('.middiv-listitem').css('height', ($(window).height() - $('.cst-menu').height() - $('.logobar').height() - 70));
            angular.element('.divforbor').css('height', ($(document).height() - $('.cst-menu').height() - $('.logobar').height() - 22));
        }
        else {
            angular.element('.middiv-listitem').css('height', 'auto');
            angular.element('.divforbor').css('height', 'auto');
        }
    };

    $scope.hoverEffect = function () {
        if (hoverwidth > 768) {
            var card = angular.element("#persona-hoverdtl");

            angular.element(".persona-hoverdtl").hover(function () {
                angular.element('#persona-hoverdtl').show();
            }, function () {
                angular.element('#persona-hoverdtl').hide();
            });

            angular.element('.midullisting > li').hover(function (event) {
                var bodyOffsets = document.body.getBoundingClientRect();
                var left = Math.round(angular.element(this).offset().left);
                var top = Math.round(angular.element(this).offset().top);
                angular.element('#persona-hoverdtl').css({ top: top - 150, left: left }).show();
            }, function () {
                angular.element('#persona-hoverdtl').hide();
            });
        }
        else {
            angular.element(".persona-hoverdtl").hover(function () {
                angular.element('#persona-hoverdtl').hide();
            });

            var foot_a = angular.element(document).height();
            var foot_b = (angular.element('footer').height() + angular.element('.logobar').height() + angular.element('.cst-menu').height() + angular.element('.leftpart_res').height() + angular.element('.middiv').height());

            if (foot_a > foot_b) {
                angular.element('footer').css('top', (angular.element(document).height() - ((angular.element('footer').height() + angular.element('.logobar').height() + angular.element('.cst-menu').height() + angular.element('.leftpart_res').height() + angular.element('.middiv').height() + 55))));
            };
        }
    }

    $scope.getBackButton = function () {

        if (hoverwidth < 768) {

            angular.element(".rsel-mycount").click(function () {
                angular.element('footer').css('top', 'auto');
                angular.element('.rsel-dict-div').slideUp('fast');
                angular.element('.rsel-grup-div').slideUp('fast');
                angular.element('.rsel-mycont-div').slideDown('slow');
                angular.element('.rightdiv').slideUp('fast');
                angular.element('.people-div').slideUp('fast');
                angular.element('.menubackbtn').removeClass('disblock');
            });

            angular.element(".rsel-dict").click(function () {
                $('footer').css('top', 'auto');
                $('.rsel-mycont-div').slideUp('fast');
                $('.rsel-grup-div').slideUp('fast');
                $('.rsel-dict-div').slideDown('slow');
                $('.rightdiv').slideUp('fast');
                $('.people-div').slideUp('fast');
                $('.menubackbtn').removeClass('disblock');
            });

            angular.element(".resl-grup").click(function () {
                $('footer').css('top', 'auto');
                $('.rsel-dict-div').slideUp('fast');
                $('.rsel-mycont-div').slideUp('fast');
                $('.rsel-grup-div').slideDown('slow');
                $('.rightdiv').slideUp('fast');
                $('.people-div').slideUp('fast');
                $('.menubackbtn').removeClass('disblock');
            });

            angular.element(".midullisting_Myacc li").click(function () {
                $('.rightdiv').slideDown('slow');
                $('.rsel-mycont-div').slideUp('fast');
                $('.menubackbtn').addClass('disblock');
                $('footer').slideUp('fast');
            });

            angular.element(".midullisting_dict li").click(function () {
                $('.rightdiv').slideDown('slow');
                $('.rsel-dict-div').slideUp('fast');
                $('.menubackbtn').addClass('disblock');
                $('footer').slideUp('fast')
            });

            angular.element(".midullisting_grup li").click(function () {
                $('.people-div').slideDown('slow');
                $('.rsel-grup-div').slideUp('fast');
                $('.menubackbtn').addClass('disblock');
                $('footer').slideUp('fast')
            });

            angular.element(".menubackbtn").click(function () {
                var option = $("#selectedOption").text();

                if (option == "My Contact") {
                    $('.rsel-mycont-div').slideDown('slow');
                    $('.rightdiv').slideUp('fast');
                    $('.menubackbtn').removeClass('disblock');
                    $('footer').slideDown('fast')

                }
                else if (option == "Directory") {
                    $('.rsel-dict-div').slideDown('slow');
                    $('.rightdiv').slideUp('fast');
                    $('.menubackbtn').removeClass('disblock');
                    $('footer').slideDown('fast')

                }
                else if (option == "Groups") {
                    $('.rsel-grup-div').slideDown('slow');
                    $('.people-div').slideUp('fast');
                    $('.menubackbtn').removeClass('disblock');
                    $('footer').slideDown('fast')

                }
            });

        } else {

            angular.element('.rsel-mycont').click(function () {
                angular.element('.rightdiv').slideUp('fast');
                angular.element('.people-div').slideUp('fast');

                angular.element('.rsel-mycont').addClass('selecected');
                angular.element('.rsel-dict').removeClass('selecected');
                angular.element('.rsel-grup').removeClass('selecected');

                angular.element('.rsel-dict-div').slideUp('fast');
                angular.element('.rsel-grup-div').slideUp('fast');
                angular.element('.rsel-mycont-div').slideDown('slow');
            });

            angular.element('.rsel-dict').click(function () {
                angular.element('.rightdiv').slideUp('fast');
                angular.element('.people-div').slideUp('fast');

                angular.element('.rsel-dict').addClass('selecected');
                angular.element('.rsel-mycont').removeClass('selecected');
                angular.element('.rsel-grup').removeClass('selecected');

                angular.element('.rsel-mycont-div').slideUp('fast');
                angular.element('.rsel-grup-div').slideUp('fast');
                angular.element('.rsel-dict-div').slideDown('slow');
            });

            angular.element('.rsel-grup').click(function () {
                angular.element('.rightdiv').slideUp('fast');
                angular.element('.people-div').slideUp('fast');

                angular.element('.rsel-grup').addClass('selecected');
                angular.element('.rsel-mycont').removeClass('selecected');
                angular.element('.rsel-dict').removeClass('selecected');

                angular.element('.rsel-mycont-div').slideUp('fast');
                angular.element('.rsel-dict-div').slideUp('fast');
                angular.element('.rsel-grup-div').slideDown('slow');
            });

            angular.element('.midullisting_Myacc').click(function () {
                angular.element('.rightdiv').slideDown('slow');
            });

            angular.element('.midullisting_dict').click(function () {
                angular.element('.rightdiv').slideDown('slow');
            });

            angular.element('.midullisting_grup').click(function () {
                angular.element('.people-div').slideDown('slow');
            });
        }
    }

    $scope.Init = function () {
        $scope.setHeight();
        $scope.getBackButton();
        $scope.hoverEffect();
        $scope.bindEvents();
    }

    $scope.bindEvents = function () {

        angular.element(document).resize(function () {
            $scope.setHeight();
            $scope.hoverEffect();
        });

        // ajax call demo
        angular.element(document).on("click", "#getData", function (e) {
            $scope.GetAll();
        });

        // view user profile
        angular.element(document).on("click", ".msper-userprof-btn", function (e) {
            angular.element(".userprofinfo").show();
        });

        // company button
        var btnflagswap = 0;
        angular.element(document).on("click", ".md-option-btn", function (e) {
            if (btnflagswap == 0) {
                angular.element('.md-option-div').css('display', 'block');
                btnflagswap = 1;
            }
            else {
                angular.element('.md-option-div').css('display', 'none');
                btnflagswap = 0;
            }
        });

        // detail tabs
        angular.element(document).on("click", ".tabs-menu a", function (e) {
            e.preventDefault();
            angular.element(this).parent().addClass("current");
            angular.element(this).parent().siblings().removeClass("current");
            var tab = angular.element(this).attr("href");
            angular.element(".tab-content").not(tab).css("display", "none");
            angular.element(tab).fadeIn();
        });

        // people tabs
        angular.element(document).on("click", ".ppl_tabmenu a", function (e) {
            e.preventDefault();
            angular.element(this).parent().addClass("current");
            angular.element(this).parent().siblings().removeClass("current");
            var tab = angular.element(this).attr("href");
            angular.element(".p_tabcontent").not(tab).css("display", "none");
            angular.element(tab).fadeIn();
        });

        angular.element(document).on("click", ".pvd-tabs-menu a", function (e) {
            e.preventDefault();
            angular.element(this).parent().addClass("current");
            angular.element(this).parent().siblings().removeClass("current");
            var tab = $(this).attr("href");
            angular.element(".pvd-tab-content").not(tab).css("display", "none");
            angular.element(tab).fadeIn();
        });

        angular.element(document).on("click", ".peoplejoinpnlbtn", function (e) {
            angular.element(".peoplejoinpnldiv").slideDown('fast');
        });

        angular.element(document).on("click", ".ppl-dtl-btn", function (e) {
            angular.element(".ppl-dtl-div").show();
        });

        angular.element(document).on("click", ".ppl-viwdtl-btn", function (e) {
            angular.element('.ppl-dtl-div').slideUp('fast');
            angular.element('.ppl-viwdtl-div').toggleClass('disblock');
        });

        angular.element(document).on("click", ".searchbox-btn", function (e) {
            angular.element('.searchbox').slideDown('slow');
        });

        angular.element(document).on("click", ".name-fld-btn", function (e) {
            angular.element('.name-fld').addClass('is-open');
            angular.element('.eml-fld').removeClass('is-open');
            angular.element('.phn-fld').removeClass('is-open');
            angular.element('.im-fld').removeClass('is-open');
            angular.element('.add-fld').removeClass('is-open');
            angular.element('.other-fld').removeClass('is-open');
            angular.element('.notes-fld').removeClass('is-open');
        });

        angular.element(document).on("click", ".eml-fld-btn", function (e) {
            angular.element('.eml-fld').addClass('is-open');
            angular.element('.name-fld').removeClass('is-open');
            angular.element('.phn-fld').removeClass('is-open');
            angular.element('.im-fld').removeClass('is-open');
            angular.element('.add-fld').removeClass('is-open');
            angular.element('.other-fld').removeClass('is-open');
            angular.element('.notes-fld').removeClass('is-open');
        });

        angular.element(document).on("click", ".phn-fld-btn", function (e) {
            angular.element('.phn-fld').addClass('is-open');
            angular.element('.name-fld').removeClass('is-open');
            angular.element('.eml-fld').removeClass('is-open');
            angular.element('.im-fld').removeClass('is-open');
            angular.element('.add-fld').removeClass('is-open');
            angular.element('.other-fld').removeClass('is-open');
            angular.element('.notes-fld').removeClass('is-open');
        });

        angular.element(document).on("click", ".im-fld-btn", function (e) {
            angular.element('.im-fld').addClass('is-open');
            angular.element('.name-fld').removeClass('is-open');
            angular.element('.eml-fld').removeClass('is-open');
            angular.element('.phn-fld').removeClass('is-open');
            angular.element('.add-fld').removeClass('is-open');
            angular.element('.other-fld').removeClass('is-open');
            angular.element('.notes-fld').removeClass('is-open');
        });

        angular.element(document).on("click", ".add-fld-btn", function (e) {
            angular.element('.add-fld').addClass('is-open');
            angular.element('.name-fld').removeClass('is-open');
            angular.element('.eml-fld').removeClass('is-open');
            angular.element('.phn-fld').removeClass('is-open');
            angular.element('.im-fld').removeClass('is-open');
            angular.element('.other-fld').removeClass('is-open');
            angular.element('.notes-fld').removeClass('is-open');
        });

        angular.element(document).on("click", ".other-fld-btn", function (e) {
            anguarl.element('.other-fld').addClass('is-open');
            anguarl.element('.name-fld').removeClass('is-open');
            anguarl.element('.eml-fld').removeClass('is-open');
            anguarl.element('.phn-fld').removeClass('is-open');
            anguarl.element('.im-fld').removeClass('is-open');
            anguarl.element('.add-fld').removeClass('is-open');
            anguarl.element('.notes-fld').removeClass('is-open');
        });

        angular.element(document).on("click", ".notes-fld-btn", function (e) {
            angular.element('.notes-fld').addClass('is-open');
            angular.element('.name-fld').removeClass('is-open');
            angular.element('.eml-fld').removeClass('is-open');
            angular.element('.phn-fld').removeClass('is-open');
            angular.element('.im-fld').removeClass('is-open');
            angular.element('.add-fld').removeClass('is-open');
            angular.element('.other-fld').removeClass('is-open');
        });

        angular.element(document).on("click", ".lpr_optbar > .init", function (e) {
            angular.element(this).closest(".lpr_optbar").children('li:not(.init)').slideDown();
        });

        var allOptions = angular.element(".lpr_optbar").children('li:not(.init)');
        angular.element(document).on("click", ".lpr_optbar > li:not(.init)", function (e) {
            allOptions.removeClass('selected');
            angular.element(this).addClass('selected');
            angular.element(".lpr_optbar").children('.init').html(angular.element(this).html());
            allOptions.slideUp();
        });

        angular.element(document).on("click", ".pdur_member", function (e) {
            angular.element('.pdur_imgs').slideDown('slow');
        });

        angular.element(document).on("click", "#submit", function (e) {
            alert("The selected Value is " + angular.element("ul").find(".selected").data("value"));
        });

    }

    $scope.Init();

    $scope.GetUserGuid = function () {
        return $scope.UserGuid;
    };

    $scope.Contact = {
        ID: "",
        GUID: "",
        ContactTypeID: "",
        RelationshipID: "",
        OfficeLocation: "",
        BusinessUnitsAudited: "",
        BerkshireProjects: "",
        BusinessPhone: "",
        SecondaryBusinessPhone: "",
        BusinessEmailAddress: "",
        MobileNumber: "",
        ActiveContact: "",
        FaxNumber: "",
        MemberOfID: "",
        JobTitle: "",
        ManagerName: "",
        AssitantName: "",
        AssistantPhone: "",
        Specialty: "",
        Address: "",
        City: "",
        StateProvince: "",
        ZipPostalCode: "",
        Country: "",
        PowerOfAttorney: "",
        PowerOfAttorneyTypeID: "",
        TaxTypeID: "",
        JurisdictionID: "",
        Form8821Authority: "",
        Birthday: "",
        WorkAnniversary: "",
        CompanyBio: "",
        TwitterHandle: "",
        Strengths: "",
        LinkedIn: "",
        Certifications: "",
        UserGuid: ""
    };

    $scope.GetAll = function () {
        svc.GetAll($scope.UserGuid, $scope.PageNumber, $scope.SortField).success(function (result) {
            console.log(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    $scope.GetDetail = function (Guid, UserGuid) {
        svc.GetDetail(Guid, UserGuid).success(function (result) {
            console.log(result);
        }).error(function (result) { console.log(result); });
    }

    $scope.AddInternal = function (Contact) {
        svc.AddInternal(Contact).success(function (response) {
            console.log(response);
        }).error(function (response) {
            console.log(response);
        });
    }

}]);