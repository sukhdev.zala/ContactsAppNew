﻿'use strict';
angular.module('corsApp', ['ngRoute'])
.config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {

    $routeProvider.when("/Home", {
        controller: "homeCtrl",
        templateUrl: "/App/Views/Home.html",
    }).when("/CorsCall", {
        controller: "corsCallCtrl",
        templateUrl: "/App/Views/CorsCall.html",
        requireADLogin: true,
    }).when("/UserData", {
        controller: "userDataCtrl",
        templateUrl: "/App/Views/UserData.html",
    }).otherwise({ redirectTo: "/Home" });

    //TODO: The endpoints setting ensures ADAL.js will automatically acquire the access tokens transparently during requests.

    //Working - API APP

    var endpoints = {
        "https://localhost:44321/": "58eb7117-2691-43c9-aab4-d7b0872aad0e"
    };

    //TODO: Replace these by your tenant and client ID after having created the Azure AD applications
    //adalProvider.init(
    //    {
    //        //Working - MAIN APP

    //        tenant: 'bhets.onmicrosoft.com',
    //        clientId: '58eb7117-2691-43c9-aab4-d7b0872aad0e',
    //        endpoints: endpoints
    //    },
    //    $httpProvider
    //    );
}]);
