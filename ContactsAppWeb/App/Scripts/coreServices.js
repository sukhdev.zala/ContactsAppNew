﻿'use strict';
angular.module('corsApp')
.factory('coreServices', ['$http', function ($http) {

    var baseURL = "https://localhost:44321/";

    var endPoints = {
        GetAll: "api/contacts/GetAll",
        GetDetail: "api/contacts/getDetail",
        AddInternal: "api/contacts/addInternal",
        AddExternal: "api/contacts/addExternal",
        EditInternal: "api/contacts/editInternal",
        EditExternal: "api/contacts/editExternal",
        Delete: "api/contacts/delete"
    };

    return {

        GetAll: function (Guid, pageNum, sortField) {
            return $http.get(baseURL + endPoints.GetAll, {
                params: {
                    Guid: Guid,
                    pageNum: pageNum,
                    sortField: sortField
                }
            }).success(function (data, status, config, headers) {
                return data;
            }).error(function () {
                return [];
            });
        },

        GetDetail: function (Guid, UserGuid) {
            return $http.get(baseURL + endPoints.GetDetail, {
                params: {
                    Guid: Guid,
                    UserGuid: UserGuid
                }
            }).success(function (data, status, config, headers) {
                return data;
            }).error(function () {
                return [];
            });
        },

        AddInternal: function (contact) {
            return $http({
                method: "post",
                url: baseURL + endPoints.AddInternal,
                data: contact,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) { }, function (response) { });
        }

    };
}]);