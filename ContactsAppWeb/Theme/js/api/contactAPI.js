﻿function setHeight() {

    var width = $(window).width();

    if (width > 768) {
        $('.middiv-listitem').css('max-height', ($(window).height() - $('.cst-menu').height() - $('.logobar').height() - 140));
        $('.middiv-listitem').css('height', ($(window).height() - $('.cst-menu').height() - $('.logobar').height() - 140));
        
        $('.divforbor').css('height', ($(document).height() - $('.cst-menu').height() - $('.logobar').height() - 22));
    }
    else {
        $('.middiv-listitem').css('height', 'auto');
        $('.divforbor').css('height', 'auto');
    }
}

function hoverEffect() {

    var hoverWidth = $(window).width();

    if (hoverWidth > 768) {
        var card = $("#persona-hoverdtl");

        $(".persona-hoverdtl").hover(function () {
            $('#persona-hoverdtl').show();
        }, function () {
            $('#persona-hoverdtl').hide();
        });

        $('.midullisting > li').hover(function (event) {
            var bodyOffsets = document.body.getBoundingClientRect();
            var left = Math.round($(this).offset().left);
            var top = Math.round($(this).offset().top);
            $('#persona-hoverdtl').css({ top: top - 150, left: left }).show();
        }, function () {
            $('#persona-hoverdtl').hide();
        });
    }
    else {

        $(".persona-hoverdtl").hover(function () {
            $('#persona-hoverdtl').hide();
        });

        var foot_a = $(document).height();
        var foot_b = ($('footer').height() + $('.logobar').height() + $('.cst-menu').height() + $('.leftpart_res').height() + $('.middiv').height());

        if (foot_a > foot_b) {
            $('footer').css('top', ($(document).height() - (($('footer').height() + $('.logobar').height() + $('.cst-menu').height() + $('.leftpart_res').height() + $('.middiv').height() + 55))));
        };
    }
}

function showContactList_BackButton() {
    $('footer').css('top', 'auto');
    $('.rsel-dict-div').slideUp('fast');
    $('.rsel-grup-div').slideUp('fast');
    $('.rsel-mycont-div').slideDown('slow');
    $('.rightdiv').slideUp('fast');
    $('.people-div').slideUp('fast');
    $('.menubackbtn').removeClass('disblock');
}

var contactsAPI = (function () {

    var hoverWidth = $(window).width();
    var btnFlag = 0;

    var contactFormValidator;

    var contactObject = {
        Guid: '47E90B39-2D82-4FD3-9365-6521176E9171',
        UserGuid: '47e90b39-2d82-4fd3-9365-6521176e9171',
        isDataAvailable: true,
        isPreviousEventComplete: true,
        pageNum: 0,
        sortField: 'FirstName',
        sortFieldText: 'First Name',
        contacts: []
    };

    var baseURL = "https://localhost:44321/";

    var endPoints = {
        GetAll: "api/contacts/GetAll",
        GetDetail: "api/contacts/getDetail",
        AddInternal: "api/contacts/addInternal",
        AddExternal: "api/contacts/addExternal",
        EditInternal: "api/contacts/editInternal",
        EditExternal: "api/contacts/editExternal",
        Delete: "api/contacts/delete",
        GetGuid: "api/contacts/getGuid"
    };

    var bindEvents = function () {

        $("#saveContactButton").click(function () {
            saveContact();
        });

        $(window).resize(function () {
            setHeight();
            //hoverEffect();
        });

        $('.contactListContainer').bind('scroll', function (e) {            
            if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {                
                $('.middiv-listitem').css('height', ($(window).height() - $('.cst-menu').height() - $('.logobar').height() - 140));
                getContacts();
            }
        });
    };

    var bindOtherEvents = function () {

        $(".optionContext li").click(function (e) {
            alert($(this).html());
        });

        $("#dialogMsgClose").click(function () {
            $("#dialogContactAdd").hide();
        });

        $(".md-option-btn").click(function () {
            $(this).toggleClass("menuOpen");
            var menu = $("#optionContext");
            menu.addClass("is-open");
        });

        // user profile
        $('.msper-userprof-btn').click(function () {
            $('.userprofinfo').show();
        });

        $(".peoplejoinpnlbtn").click(function () {
            $('.peoplejoinpnldiv').slideDown('fast');
        });

        $('.ppl-dtl-btn').click(function () {
            $('.ppl-dtl-div').show();
        });

        $('.ppl-viwdtl-btn').click(function () {
            $('.ppl-dtl-div').slideUp('fast');
            $('.ppl-viwdtl-div').toggleClass('disblock');
        });

        $('.searchbox-btn').click(function () {
            $('.searchbox').slideDown('slow');
        });

        $('.name-fld-btn').click(function () {
            $('.name-fld').addClass('is-open');
            $('.eml-fld').removeClass('is-open');
            $('.phn-fld').removeClass('is-open');
            $('.im-fld').removeClass('is-open');
            $('.add-fld').removeClass('is-open');
            $('.other-fld').removeClass('is-open');
            $('.notes-fld').removeClass('is-open');
        });

        $('.eml-fld-btn').click(function () {
            $('.eml-fld').addClass('is-open');
            $('.name-fld').removeClass('is-open');
            $('.phn-fld').removeClass('is-open');
            $('.im-fld').removeClass('is-open');
            $('.add-fld').removeClass('is-open');
            $('.other-fld').removeClass('is-open');
            $('.notes-fld').removeClass('is-open');
        });

        $('.phn-fld-btn').click(function () {
            $('.phn-fld').addClass('is-open');
            $('.name-fld').removeClass('is-open');
            $('.eml-fld').removeClass('is-open');
            $('.im-fld').removeClass('is-open');
            $('.add-fld').removeClass('is-open');
            $('.other-fld').removeClass('is-open');
            $('.notes-fld').removeClass('is-open');
        });

        $('.im-fld-btn').click(function () {
            $('.im-fld').addClass('is-open');
            $('.name-fld').removeClass('is-open');
            $('.eml-fld').removeClass('is-open');
            $('.phn-fld').removeClass('is-open');
            $('.add-fld').removeClass('is-open');
            $('.other-fld').removeClass('is-open');
            $('.notes-fld').removeClass('is-open');
        });

        $('.add-fld-btn').click(function () {
            $('.add-fld').addClass('is-open');
            $('.name-fld').removeClass('is-open');
            $('.eml-fld').removeClass('is-open');
            $('.phn-fld').removeClass('is-open');
            $('.im-fld').removeClass('is-open');
            $('.other-fld').removeClass('is-open');
            $('.notes-fld').removeClass('is-open');
        });

        $('.other-fld-btn').click(function () {
            $('.other-fld').addClass('is-open');
            $('.name-fld').removeClass('is-open');
            $('.eml-fld').removeClass('is-open');
            $('.phn-fld').removeClass('is-open');
            $('.im-fld').removeClass('is-open');
            $('.add-fld').removeClass('is-open');
            $('.notes-fld').removeClass('is-open');
        });

        $('.notes-fld-btn').click(function () {
            $('.notes-fld').addClass('is-open');
            $('.name-fld').removeClass('is-open');
            $('.eml-fld').removeClass('is-open');
            $('.phn-fld').removeClass('is-open');
            $('.im-fld').removeClass('is-open');
            $('.add-fld').removeClass('is-open');
            $('.other-fld').removeClass('is-open');
        });

        $(".lpr_optbar").on("click", ".init", function () {
            $(this).closest(".lpr_optbar").children('li:not(.init)').slideDown();
        });

        var allOptions = $(".lpr_optbar").children('li:not(.init)');
        $(".lpr_optbar").on("click", "li:not(.init)", function () {
            allOptions.removeClass('selected');
            $(this).addClass('selected');
            $(".lpr_optbar").children('.init').html($(this).html());
            allOptions.slideUp();
        });

        $("#submit").click(function () {
            alert("The selected Value is " + $("ul").find(".selected").data("value"));
        });

        $('.pdur_member').click(function () {
            $('.pdur_imgs').slideDown('slow');
        });
    };

    var dialogEvents = function () {
        $(".js-DialogAction--open").each(function () {
            $(this).on('click', function () {
                var targetId = $(this).data("target");
                $("#" + targetId).show();
            });
        });

        $(".js-DialogAction--close").each(function () {
            $(this).on('click', function () {
                var targetId = $(this).data("target");
                $("#" + targetId).hide();
            });
        });

        $(".btnDeleteContact").click(function () {
            var action = $(this).data("action");
            var target = $(this).closest(".ms-Dialog");
            $(target).hide();
        });
    };

    var sortContacts = function (sortField) {
        $("#selctedOptionText").text("By " + contactObject.sortFieldText);
        contactObject.pageNum = 0;
        contactObject.contacts = [];
        clearContactsList();
        getContacts();
    };

    var otherCloseEvents = function () {
        var currentPanel = "";

        $(document).ready(function () {
            $(".optionMenuItem").on("click", function () {

                var option = $(this).data("sort");
                var optionText = $(this).data("sort-text");

                contactObject.sortField = option;
                contactObject.sortFieldText = optionText;

                $("#optionContext").removeClass("is-open");
                $("#moreOptionContext").removeClass("is-open");
                sortContacts(option);
            });
            initCalendar();
            $(".ms-PanelAction-close").on("click", function () {
                var panelId = $(this).data("panel-id");
                $("#" + panelId).addClass("ms-Panel-animateOut");
            });

            $(".ms-PanelAction-open").on("click", function () {

                var panelId = $(this).data("panel-id");
                currentPanel = panelId;

                $("#" + panelId).addClass("is-open");
                $("#" + panelId).addClass("ms-Panel-animateIn");
            });

        });

        $(document).mouseup(function (e) {
            var container = $(".userprofinfo");

            if (!$("a").is(e.target) &&
                !container.is(e.target) && container.has(e.target).length === 0) {
                container.hide();
            }
        });

        // sort option context menu close
        $(document).mouseup(function (e) {
            var container = $(".md-option-div");
            var openButton = $(".md-option-btn");

            if (!$("a").is(e.target) &&
                !container.is(e.target) && container.has(e.target).length === 0 &&
                !openButton.is(e.target) && openButton.has(e.target).length === 0) {
                container.hide();
            }

        });




        $("#groupInfoButton").click(function () {
            $("#groupInfo").show();
        });

        $(document).mouseup(function (e) {

            var container = $("#groupInfo");
            var gpButton = $("#groupInfoButton");

            if (!$("a").is(e.target) &&
                !container.is(e.target) && container.has(e.target).length === 0 &&
                !gpButton.is(e.target) && gpButton.has(e.target).length === 0) {
                container.hide();
            }
        });

        $(document).mouseup(function (e) {

            var searbhBox = $(".searchbox");
            var searchButton = $(".searchbox-btn");

            if (!$("a").is(e.target) &&
                !searbhBox.is(e.target) && searbhBox.has(e.target).length === 0 &&
                !searchButton.is(e.target) && searchButton.has(e.target).length === 0) {
                searbhBox.hide();
            }
        });

        $(".ms-Overlay").click(function (e) {
            $("#" + currentPanel).addClass("ms-Panel-animateOut");
        });
    };

    var initCalendar = function () {
        $(".ms-DatePicker").DatePicker();
        $("div.ms-DatePicker-monthPicker > .ms-DatePicker-optionGrid").unbind();
    };

    var initFabricUI = function () {
        // file picker
        if ($('.ms-FilePicker').length > 0) {
            $('.ms-FilePicker').css({
                "position": "absolute !important"
            });

            $('.ms-Panel').FilePicker();
        } else {
            if ($.fn.NavBar) {
                $('.ms-NavBar').NavBar();
            }
        }

        // nav bar
        if (typeof fabric !== "undefined") {
            if ('NavBar' in fabric) {
                var elements = document.querySelectorAll('.ms-NavBar');
                var i = elements.length;
                var component;
                while (i--) {
                    component = new fabric['NavBar'](elements[i]);
                }
            }
        }

        // dialog
        if (typeof fabric !== "undefined") {
            if ('Dialog' in fabric) {
                var elements = document.querySelectorAll('.ms-Dialog');
                var i = elements.length;
                var component;
                while (i--) {
                    component = new fabric['Dialog'](elements[i]);
                }
            }
        }

        // File Picker demo fixes
        if ($('.ms-FilePicker').length > 0) {
            $('.ms-FilePicker').css({
                "position": "absolute !important"
            });

            $('.ms-Panel').FilePicker();
        } else {
            if ($.fn.ContextualMenu) {
                $('.ms-ContextualMenu').ContextualMenu();
            }
        }

        // context menu
        if (typeof fabric !== "undefined") {
            if ('ContextualMenu' in fabric) {
                var elements = document.querySelectorAll('.ms-ContextualMenu');
                var i = elements.length;
                var component;
                while (i--) {
                    component = new fabric['ContextualMenu'](elements[i]);
                }
            }
        }

        // File Picker demo fixes
        if ($('.ms-FilePicker').length > 0) {
            $('.ms-FilePicker').css({
                "position": "absolute !important"
            });

            $('.ms-Panel').FilePicker();
        } else {
            if ($.fn.Panel) {
                $('.ms-Panel').Panel();
            }
        }

        // panel
        if (typeof fabric !== "undefined") {
            if ('Panel' in fabric) {
                var elements = document.querySelectorAll('.ms-Panel');
                var i = elements.length;
                var component;
                while (i--) {
                    component = new fabric['Panel'](elements[i]);
                }
            }
        }

        // File Picker demo fixes
        if ($('.ms-FilePicker').length > 0) {
            $('.ms-FilePicker').css({
                "position": "absolute !important"
            });

            $('.ms-Panel').FilePicker();
        } else {
            if ($.fn.Dropdown) {
                $('.ms-Dropdown').Dropdown();
            }
        }

        // dropdown
        if (typeof fabric !== "undefined") {
            if ('Dropdown' in fabric) {
                var elements = document.querySelectorAll('.ms-Dropdown');
                var i = elements.length;
                var component;
                while (i--) {
                    component = new fabric['Dropdown'](elements[i]);
                }
            }
        }

        // File Picker demo fixes
        if ($('.ms-FilePicker').length > 0) {
            $('.ms-FilePicker').css({
                "position": "absolute !important"
            });

            $('.ms-Panel').FilePicker();
        } else {
            if ($.fn.PersonaCard) {
                $('.ms-PersonaCard').PersonaCard();
            }
        }

        // persona card
        if (typeof fabric !== "undefined") {
            if ('PersonaCard' in fabric) {
                var elements = document.querySelectorAll('.ms-PersonaCard');
                var i = elements.length;
                var component;
                while (i--) {
                    component = new fabric['PersonaCard'](elements[i]);
                }
            }
        }

         //Spinner File Picker demo fixes
        if ($('.ms-FilePicker').length > 0) {
            $('.ms-FilePicker').css({
                "position": "absolute !important"
            });

            $('.ms-Panel').FilePicker();
        } else {
            if ($.fn.Spinner) {
                $('.ms-Spinner').Spinner();
            }
        }

        // spinner
        if (typeof fabric !== "undefined") {
            if ('Spinner' in fabric) {
                var elements = document.querySelectorAll('.ms-Spinner');
                var i = elements.length;
                var component;
                while (i--) {
                    component = new fabric['Spinner'](elements[i]);
                }
            }
        }
    };

    var initTabs = function () {
        $(".tabs-menu a").click(function (event) {
            event.preventDefault();
            $(this).parent().addClass("current");
            $(this).parent().siblings().removeClass("current");
            var tab = $(this).attr("href");
            $(".tab-content").not(tab).css("display", "none");
            $(tab).fadeIn();
        });

        $(".ppl_tabmenu a").click(function (event) {
            event.preventDefault();
            $(this).parent().addClass("current");
            $(this).parent().siblings().removeClass("current");
            var tab = $(this).attr("href");
            $(".p_tabcontent").not(tab).css("display", "none");
            $(tab).fadeIn();
        });

        $(".pvd-tabs-menu a").click(function (event) {
            event.preventDefault();
            $(this).parent().addClass("current");
            $(this).parent().siblings().removeClass("current");
            var tab = $(this).attr("href");
            $(".pvd-tab-content").not(tab).css("display", "none");
            $(tab).fadeIn();
        });
    };

    var initWithBackButton = function () {
        $(".rsel-mycont").click(function () {
            showContactList_BackButton();
        });

        $(".rsel-dict").click(function () {
            $('footer').css('top', 'auto');
            $('.rsel-mycont-div').slideUp('fast');
            $('.rsel-grup-div').slideUp('fast');
            $('.rsel-dict-div').slideDown('slow');
            $('.rightdiv').slideUp('fast');
            $('.people-div').slideUp('fast');
            $('.menubackbtn').removeClass('disblock');

        });

        $('.resl-grup').click(function () {
            $('footer').css('top', 'auto');
            $('.rsel-dict-div').slideUp('fast');
            $('.rsel-mycont-div').slideUp('fast');
            $('.rsel-grup-div').slideDown('slow');
            $('.rightdiv').slideUp('fast');
            $('.people-div').slideUp('fast');
            $('.menubackbtn').removeClass('disblock');

        });

        $('.midullisting_Myacc li').click(function () {
            $('.rightdiv').slideDown('slow');
            $('.rsel-mycont-div').slideUp('fast');
            $('.menubackbtn').addClass('disblock');
            $('footer').slideUp('fast');
        });

        $('.midullisting_dict li').click(function () {
            $('.rightdiv').slideDown('slow');
            $('.rsel-dict-div').slideUp('fast');
            $('.menubackbtn').addClass('disblock');
            $('footer').slideUp('fast')
        });

        $('.midullisting_grup li').click(function () {
            $('.people-div').slideDown('slow');
            $('.rsel-grup-div').slideUp('fast');
            $('.menubackbtn').addClass('disblock');
            $('footer').slideUp('fast')
        });

        $('.menubackbtn').click(function () {
            var option = $("#selectedOption").text();

            if (option == "My Contact") {
                $('.rsel-mycont-div').slideDown('slow');
                $('.rightdiv').slideUp('fast');
                $('.menubackbtn').removeClass('disblock');
                $('footer').slideDown('fast')

            }
            else if (option == "Directory") {
                $('.rsel-dict-div').slideDown('slow');
                $('.rightdiv').slideUp('fast');
                $('.menubackbtn').removeClass('disblock');
                $('footer').slideDown('fast')

            }
            else if (option == "Groups") {
                $('.rsel-grup-div').slideDown('slow');
                $('.people-div').slideUp('fast');
                $('.menubackbtn').removeClass('disblock');
                $('footer').slideDown('fast')

            }
        });
    };

    var initWithOutBackButton = function () {
        $('.rsel-mycont').click(function () {
        
            // load for the first time
            contactObject.pageNum = 0;
            clearContactsList();
            getContacts();

            $('.rightdiv').slideUp('fast');
            $('.people-div').slideUp('fast');

            $('.rsel-mycont').addClass('selecected');
            $('.rsel-dict').removeClass('selecected');
            $('.rsel-grup').removeClass('selecected');

            $('.rsel-dict-div').slideUp('fast');
            $('.rsel-grup-div').slideUp('fast');
            $('.rsel-mycont-div').slideDown('slow');
        });

        $('.rsel-dict').click(function () {
            $('.rightdiv').slideUp('fast');
            $('.people-div').slideUp('fast');

            $('.rsel-dict').addClass('selecected');
            $('.rsel-mycont').removeClass('selecected');
            $('.rsel-grup').removeClass('selecected');

            $('.rsel-mycont-div').slideUp('fast');
            $('.rsel-grup-div').slideUp('fast');
            $('.rsel-dict-div').slideDown('slow');
        });

        $('.rsel-grup').click(function () {
            $('.rightdiv').slideUp('fast');
            $('.people-div').slideUp('fast');

            $('.rsel-grup').addClass('selecected');
            $('.rsel-mycont').removeClass('selecected');
            $('.rsel-dict').removeClass('selecected');

            $('.rsel-mycont-div').slideUp('fast');
            $('.rsel-dict-div').slideUp('fast');
            $('.rsel-grup-div').slideDown('slow');
        });

        $(".midullisting_Myacc").on("click", "li", function () {
            var guid = $(this).data("userguid");
            clearContactDetailAndUpdate(guid);
            $('.rightdiv').slideDown('slow');
        });

        $('.midullisting_dict').click(function () {
            $('.rightdiv').slideDown('slow');
        });

        $('.midullisting_grup').click(function () {
            $('.people-div').slideDown('slow');
        });
    };

    var clearContactsList = function () {
        $(".midullisting_Myacc").empty();
    };

    var getContacts = function () {
        if (contactObject.isDataAvailable == true) {

            if (contactObject.pageNum == 1) {
                clearContactsList();
            }

            if (contactObject.isPreviousEventComplete && contactObject.isDataAvailable) {
                isPreviousEventComplete = false;
                contactObject.pageNum += 1;

                $.ajax({
                    url: baseURL + endPoints.GetAll,
                    type: 'get',
                    async: false,
                    dataType: 'json',
                    data: { Guid: contactObject.Guid, pageNum: contactObject.pageNum, sortField: contactObject.sortField },
                    success: function (result) {
                        isPreviousEventComplete = true;
                        if (result.Status == 1) {
                            injectNewContacts(result);
                        }
                        else if (result.Status == 2) {
                            contactObject.pageNum = -1;
                            console.log("data not available");
                            contactObject.isDataAvailable = false;
                        }
                    },
                    error: function (requestObject, error, errorThrown) {
                        logError("getContacts", error);
                        isPreviousEventComplete = true;
                    }
                });
            }
        }
    };

    var getContactDetail = function (UserGuid) {
        $.ajax({
            url: baseURL + endPoints.GetDetail,
            type: 'get',
            async: false,
            dataType: 'json',
            data: { Guid: contactObject.Guid, UserGuid: UserGuid },
            success: function (result) {
                if (result.Status == 1) {
                    $.each(result.Data, function (key, value) {
                        if (value != null) {
                            if (key != "BirthDate" && key != "ActiveContact") {
                                if (key == "PreferredName") {
                                    console.log("inside prefered");
                                    $("#dpName").text(value);
                                }
                                else if (key == "JobTitle") {
                                    console.log("inside job title");
                                    $("#dpJobTitle").text(value);
                                }
                                else {
                                    $("#lbl" + key).text(value);
                                }
                            }
                        }
                    });
                }
                else {
                    logError("getContactDetail", result.Message);
                }
            },
            error: function (requestObject, error, errorThrown) {
                logError("getContactDetail", error);
            }
        });
    };

    var clearLables = function () {
        $("#lblFirstName").text("");
        $("#lblLastName").text("");
        $("#lblPreferredName").text("");

        $("#lblMobile").text("");
        $("#lblEmail").text("");
        $("#lblBirthDateString").text("");
        //$("#lblActiveContact).text("");
        $("#lblFax").text("");
        $("#lblAddress").text("");
        $("#lblCity").text("");
        $("#lblState").text("");
        $("#lblCountry").text("");
        $("#lblZipCode").text("");

        $("#lblJobTitle").text("");
        //$("#lblDepartment").text("");
        $("#lblCompanyBio").text("");
        $("#lblOfficeLocation").text("");
        $("#lblBusinessPhone").text("");
        $("#lblSecondaryBusinessPhone").text("");
        $("#lblBusinessEmailAddress").text("");

        $("#lblBerkshireProjects").text("");
        $("#lblBusinessUnit").text("");
        $("#lblAssistantName").text("");
        $("#lblBusinessUnitsAudited").text("");

        $("#lblTwitterHandle").text("");
        $("#lblLinkedIn").text("");
        $("#lblSpeciality").text("");
        $("#lblStrength").text("");
        $("#lblCertifications").text("");

        $("#dpName").text("");
        $("#dpJobTitle").text("");
    };

    var clearContactDetailAndUpdate = function (UserGuid) {
        clearLables();
        getContactDetail(UserGuid);
    };

    var logError = function (originFunction, errorMessage) {
        console.log("Error (" + originFunction + "): " + errorMessage);
    };

    var injectNewContacts = function (contacts) {
        $.each(contacts, function (index, item) {
            if (index != "Status" && index != "Message") {
                $("#contactTemplate").tmpl(item).appendTo(".midullisting_Myacc");
            }
        });
    };

    var resetAddControlPanel = function () {

    };

    var saveContact = function (e) {

        var isValid = $("#contactForm").valid();
        if (isValid) {
            $.ajax({
                url: baseURL + endPoints.AddExternal,
                type: 'post',
                dataType: 'json',
                data: $("#contactForm").serialize(),
                async: true,
                success: function (response) {
                    if (response.IsModelError == true) {
                        console.log(response.Errors);
                        contactFormValidator.showErrros(response.Errors);
                        resetAddControlPanel();
                    }
                    else if (response.IsModelError == false) {
                        if (response.Status == true) {
                            $("#panelContactAdd").removeClass("is-open");
                            errorDialog("error", "Error", response.Message);
                        }
                        else if (response.Status == false) {
                            errorDialog("error", "Error", response.Message);
                        }
                    }
                },
                error: function (xhr, options, error) {
                    errorDialog("error", "Error", error);
                }
            });
        } else {
            e.preventDefault();
        }
    };

    var errorDialog = function (type, title, message) {
        var color = "#0078d7";
        if (type == "error") {
            color = "red";
        }

        $("#dialogMessageHeader").css("backgorund-color", color);
        $("#dialogMessageTitle").text(title);
        $("#dialogMessageBody").text(message);
    };

    var deleteContact = function (Guid, UserGuid) {
        $.ajax({});
    };

    var editContact = function () {

    };

    var validateAddContactFormFromResponse = function () {

    };

    var InitValidations = function () {
        contactFormValidator = $("#contactForm").validate({
            rules: {
                ContactType: { required: true },
                Relationship: { required: true },
                FirstName: { required: true },
                LastName: { required: true },
                PreferredName: { required: true },
                OfficeLocation: { required: true },
                BusinessUnitsAudited: { required: true },
                BerkshireProjects: { required: true },
                BusinessPhone: { required: true, digits: true },
                SecondaryBusinessPhone: { required: true, digits: true },
                BusinessEmailAddress: { required: true, email: true }
            }
        });
    };

    var InitPlugins = function () {
    };

    var Init = function () {

        if (hoverWidth < 768) {
            initWithBackButton();
        }
        else {
            initWithOutBackButton();
        }

        bindEvents();
        bindOtherEvents();

        dialogEvents();
        otherCloseEvents();

        setHeight();
        //hoverEffect();

        initFabricUI();
        initTabs();
        InitPlugins();


        InitValidations();
    };

    return { Init: Init, contactObject: contactObject };
}());