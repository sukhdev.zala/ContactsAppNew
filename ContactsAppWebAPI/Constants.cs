﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContactsAppWebAPI
{
    public class Constants
    {
        public Constants()
        {
            ContactTypes = new Dictionary<int, string> 
            { 
                {1, "BHETS Portal User"},
                {2, "Tax External Contact"},
                {3, "IRS Audit Contact"},
                {4, "Berkshire Portal User"}
            };

            Relationships = new Dictionary<int, string> 
            {
                {1, "Berkshire Corporate"},
                {2, "Berkshire Company"},
                {3, "Contractor"},
                {4, "CPA Firm"},
                {5, "Internal Revenue Service"},
                {6, "Law Firm"}
            };

            MembersOf = new Dictionary<int, string> 
            { 
                {1, "IP Contact"},
                {2, "Indirect Tax"},
                {3, "International Tax"},
                {4, "BH Tax Group"},
                {5, "BH Tax Group Audits"},
                {6, "BH Tax Group Compliance"}
            };

            PowerOfAttorney = new Dictionary<int, string> 
            { 
                { 1, "YES" }, 
                { 0, "NO" }
            };

            PowerOfAttorneyTypes = new Dictionary<int, string> 
            { 
                {1, "Tax Type (Income, Indirect)"},
                {2, "Jurisdiction"}
            };

            TaxTypes = new Dictionary<int, string> 
            {
                {1, "Income"},
                {2, "Indirect"},
                {3, "Payroll"}
            };

            Jurisdiction = new Dictionary<int, string> 
            {
                {1, "Federal"},
                {2, "State"}
            };

            Form821Authority = new Dictionary<int, string>
            {
                {1, "YES"},
                {0, "NO"}
            };
        }

        public Dictionary<int, string> ContactTypes { get; set; }
        public Dictionary<int, string> Relationships { get; set; }
        public Dictionary<int, string> MembersOf { get; set; }
        public Dictionary<int, string> PowerOfAttorney { get; set; }
        public Dictionary<int, string> PowerOfAttorneyTypes { get; set; }
        public Dictionary<int, string> TaxTypes { get; set; }
        public Dictionary<int, string> Jurisdiction { get; set; }
        public Dictionary<int, string> Form821Authority { get; set; }
    }
}