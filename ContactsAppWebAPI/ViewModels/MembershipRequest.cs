﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContactsAppWebAPI.ViewModels
{
    public class MembershipRequest
    {
        public int Id { get; set; }
        public Guid UserGuid { get; set; }
        public bool IsGroup { get; set; }
        public int GroupSubGroupId { get; set; }
    }
}