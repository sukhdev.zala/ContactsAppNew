﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContactsAppWebAPI.ViewModels
{
    public class ContactListViewModel
    {
        public int Id { get; set; }
        public Guid UserGuid { get; set; }
        public string PreferredName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ImagePath { get; set; }
        public string JobTitle { get; set; }
    }
}