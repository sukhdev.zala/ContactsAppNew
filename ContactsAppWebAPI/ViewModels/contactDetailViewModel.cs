﻿using ContactsAppWeb.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContactsAppWebAPI.ViewModels
{
    public class ContactDetailViewModel
    {
        public ContactDetailViewModel()
        {
            GroupsSubGroups = new List<ContactGroupSubGroupViewModel>();
        }

        public List<ContactGroupSubGroupViewModel> GroupsSubGroups { get; set; }

        #region General Details
        public int ID { get; set; }
        public Guid Guid { get; set; }
        public Guid UserGuid { get; set; }
        public int ContactType { get; set; }
        #endregion

        #region Contact Tab
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PreferredName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public DateTime? BirthDate { get; set; }
        public string BirthDateString { get; set; }
        public bool ActiveContact { get; set; }
        public string Fax { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public int? ZipCode { get; set; }
        #endregion

        #region Organization Tab
        public string JobTitle { get; set; }
        public string Department { get; set; }
        public string CompanyBio { get; set; }
        public string OfficeLocation { get; set; }
        public string BusinessPhone { get; set; }
        public string SecondaryBusinessPhone { get; set; }
        public string BusinessEmailAddress { get; set; }
        #endregion

        #region Project Tab
        public string BerkshireProjects { get; set; }
        public string BusinessUnit { get; set; }
        public string ManagerName { get; set; }
        public string AssistantName { get; set; }
        public string BusinessUnitsAudited { get; set; }
        #endregion

        #region Other Tab
        public string TwitterHandle { get; set; }
        public string LinkedIn { get; set; }
        public string Speciality { get; set; }
        public string Strength { get; set; }
        public string Certifications { get; set; }
        #endregion
    }

    public class ContactGroupSubGroupViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsGroup { get; set; }
    }
}