﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContactsAppWebAPI.ViewModels
{
    public class GroupListViewModel
    {
        public GroupListViewModel()
        {
            SubGroups = new List<SubGroupViewModel>();
        }
        public int Id { get; set; }
        public string GroupName { get; set; }
        public List<SubGroupViewModel> SubGroups { get; set; }
    }

    public class SubGroupViewModel
    {
        public int Id { get; set; }
        public int GroupId { get; set; }
        public string SubGroupName { get; set; }
    }
}