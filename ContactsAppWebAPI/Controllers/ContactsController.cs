﻿using ContactsAppWeb.Core.Models;
using ContactsAppWebAPI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using PagedList;
using PagedList.Mvc;
using ContactsAppWebAPI;
using AutoMapper;
using ContactsAppWeb.Core.ApiResponses;
using ContactsAppWebAPI;

namespace ContactsAppWebAPI.Controllers
{
    public class ContactsController : ApiController
    {
        ContactsAppContext context;

        public ContactsController()
        {
            context = new ContactsAppContext();
        }

        [HttpGet]
        [Route("api/contacts/GetAll")]
        public ApiListResponse<ContactListViewModel> GetAll(Guid Guid, string pageNum, string sortField)
        {
            int pageNumber = 1;
            int pageSize = 20;
            var apiResponse = new ApiListResponse<ContactListViewModel>();

            if (!String.IsNullOrEmpty(pageNum))
                Int32.TryParse(pageNum, out pageNumber);

            try
            {
                var contacts = from c in context.Contacts
                               join u in context.Users on c.UserGuid equals u.GUID
                               where c.GUID == Guid
                               select new
                               {
                                   c.ID,
                                   u.Image,
                                   u.PreferredName,
                                   u.FirstName,
                                   u.LastName,
                                   u.GUID,
                                   c.CompanyBio,
                                   c.City,
                                   c.JobTitle,
                                   Added = DateTime.UtcNow
                               };

                if (!String.IsNullOrEmpty(sortField))
                {
                    if (sortField.Equals("PreferredName", StringComparison.OrdinalIgnoreCase))
                        contacts = contacts.OrderBy(c => c.PreferredName);

                    else if (sortField.Equals("FirstName", StringComparison.OrdinalIgnoreCase))
                        contacts = contacts.OrderBy(c => c.FirstName);

                    else if (sortField.Equals("LastName", StringComparison.OrdinalIgnoreCase))
                        contacts = contacts.OrderBy(c => c.LastName);

                    else if (sortField.Equals("Company", StringComparison.OrdinalIgnoreCase))
                        contacts = contacts.OrderBy(c => c.CompanyBio);

                    else if (sortField == "City")
                        contacts = contacts.OrderBy(c => c.City);

                    else if (sortField == "Recent")
                        contacts = contacts.OrderBy(c => c.Added);

                    else contacts = contacts.OrderBy(c => c.PreferredName);
                }

                var temp = contacts.Select(d => new ContactListViewModel
                                                            {
                                                                Id = d.ID,
                                                                ImagePath = d.Image,
                                                                PreferredName = d.PreferredName,
                                                                FirstName = d.FirstName,
                                                                LastName = d.LastName,
                                                                UserGuid = d.GUID,
                                                                JobTitle = d.JobTitle
                                                            });

                IPagedList<ContactListViewModel> data = temp.ToPagedList(pageNumber, pageSize);
                if (data.Any())
                {
                    apiResponse.Status = 1;
                    apiResponse.Data = data.ToList();
                }
                else
                {
                    apiResponse.Status = 2;
                }
                return apiResponse;
            }
            catch (Exception ex)
            {
                apiResponse.Status = 0;
                apiResponse.Message = ex.Message;
                return apiResponse;
            }
        }

        [HttpGet]
        [Route("api/contacts/getDetail")]
        public ApiObjectResponse<ContactDetailViewModel> GetDetail(Guid Guid, Guid UserGuid)
        {
            var apiResponse = new ApiObjectResponse<ContactDetailViewModel>();

            try
            {
                #region Contact Detail
                var contact = from c in context.Contacts
                              join u in context.Users on c.UserGuid equals u.GUID
                              where c.GUID == Guid && u.GUID == UserGuid
                              select new ContactDetailViewModel
                              {
                                  ID = c.ID,
                                  Guid = c.GUID,
                                  UserGuid = u.GUID,
                                  ContactType = c.ContactTypeID,

                                  FirstName = u.FirstName,
                                  LastName = u.LastName,
                                  PreferredName = u.PreferredName,

                                  Mobile = c.MobileNumber,
                                  Email = u.Email,
                                  BirthDate = c.Birthday,
                                  //ActiveContact = u.Active,
                                  Fax = c.FaxNumber,
                                  Address = c.Address,
                                  City = c.City,
                                  State = c.StateProvince,
                                  Country = c.Country,
                                  ZipCode = c.ZipPostalCode,

                                  JobTitle = c.JobTitle,
                                  //Department    //Where does its value come from
                                  CompanyBio = c.CompanyBio,
                                  OfficeLocation = c.OfficeLocation,
                                  BusinessPhone = c.BusinessPhone,
                                  SecondaryBusinessPhone = c.SecondaryBusinessPhone,
                                  BusinessEmailAddress = c.BusinessEmailAddress,

                                  BerkshireProjects = c.BerkshireProjects,
                                  BusinessUnit = u.BU.Code, //Ensure LazyLoad
                                  AssistantName = c.AssitantName,
                                  BusinessUnitsAudited = c.BusinessUnitsAudited,

                                  TwitterHandle = c.TwitterHandle,
                                  LinkedIn = c.LinkedIn,
                                  Speciality = c.Specialty,
                                  Strength = c.Strengths,
                                  Certifications = c.Certifications
                              };

                var data = contact.FirstOrDefault();
                #endregion

                #region Group/SubGroups
                var groups = context.GroupMembers.Include("Group").Where(c => c.UserGuid == UserGuid).Select(c =>
                    new ContactGroupSubGroupViewModel
                    {
                        Id = c.ID,
                        IsGroup = true,
                        Name = c.Group.GroupName
                    }).ToList();

                var subGroups = context.SubGroupMembers.Include("SubGroup").Where(c => c.UserGuid == UserGuid).Select(c =>
                    new ContactGroupSubGroupViewModel
                    {
                        Id = c.ID,
                        IsGroup = false,
                        Name = c.SubGroup.SubGroupName
                    }).ToList();

                var combinedList = groups.Union(subGroups).ToList();
                #endregion

                if (data != null)
                {
                    if (data.BirthDate.HasValue)
                    {
                        data.BirthDateString = data.BirthDate.Value.ToString("MMMM") + ", " + data.BirthDate.Value.Day;
                    }
                    data.GroupsSubGroups = combinedList;
                    apiResponse.Status = true;
                    apiResponse.Data = data;
                    return apiResponse;
                }
                return apiResponse;
            }
            catch (Exception ex)
            {
                apiResponse.Status = false;
                apiResponse.Message = ex.Message;
                return apiResponse;
            }
        }

        [HttpPost]
        [Route("api/contacts/addInternal")]
        public ApiModelResponse AddInternal(Contact model)
        {
            ApiModelResponse apiResponse = new ApiModelResponse();
            try
            {
                if (model.UserGuid == null)
                {
                    ModelState.AddModelError("UserGuid", "User not specified");
                    var errors = ModelState.GetModelErrors();
                }

                if (ModelState.IsValid)
                {
                    context.Contacts.Add(model);
                    context.SaveChanges();

                    apiResponse.Status = true;
                    return apiResponse;
                }
                else
                {
                    var errors = ModelState.GetModelErrors();
                    apiResponse.IsModelError = true;
                    apiResponse.Status = false;
                    apiResponse.Errors = errors;
                    return apiResponse;
                }
            }
            catch (Exception ex)
            {
                apiResponse.Status = false;
                apiResponse.IsModelError = false;
                apiResponse.Message = ex.Message;
                return apiResponse;
            }
        }

        [HttpPost]
        [ValidateModel]
        [Route("api/contacts/addExternal")]
        public ApiModelResponse AddExternal(Contact model)
        {
            ApiModelResponse apiResponse = new ApiModelResponse();
            try
            {
                if (ModelState.IsValid)
                {
                    apiResponse.Status = true;
                    apiResponse.IsModelError = false;
                    //context.Contacts.Add(model);
                    //context.SaveChanges();
                    return apiResponse;
                }
                else
                {
                    var errors = ModelState.GetModelErrors();
                    apiResponse.Status = false;
                    apiResponse.IsModelError = true;
                    apiResponse.Errors = errors;
                    return apiResponse;
                }
            }
            catch (Exception ex)
            {
                apiResponse.Status = false;
                apiResponse.IsModelError = false;
                apiResponse.Errors = null;
                apiResponse.Message = ex.Message;
                return apiResponse;
            }
        }

        [HttpPost]
        [Route("api/contacts/editInternal")]
        public dynamic EditInternal(Contact model)
        {
            if (model.UserGuid == null)
            {
                ModelState.AddModelError("UserGuid", "User not specified");
                var errors = ModelState.GetModelErrors();
                return new { Status = false, Errors = errors };
            }

            if (ModelState.IsValid)
            {
                var contact = context.Contacts.Where(c => c.UserGuid == model.UserGuid && c.GUID == model.GUID && c.ActiveContact).FirstOrDefault();
                if (contact == null)
                {
                    ModelState.AddModelError("UserGuid", "User does not exists");
                    var errors = ModelState.GetModelErrors();
                    return new { Status = false, Errors = errors };
                }
                contact = Mapper.Map<Contact>(model);
                context.SaveChanges();
                return new { success = true };
            }
            else
            {
                var errors = ModelState.GetModelErrors();
                return new
                {
                    Status = false,
                    Errors = errors
                };
            }
        }

        [HttpPost]
        [Route("api/contacts/editExternal")]
        public dynamic EditExternal(Contact model)
        {
            if (ModelState.IsValid)
            {
                var contact = context.Contacts.Where(c => c.UserGuid == model.UserGuid && c.GUID == model.GUID && c.ActiveContact).FirstOrDefault();
                if (contact == null)
                {
                    ModelState.AddModelError("UserGuid", "User does not exists");
                    var errors = ModelState.GetModelErrors();
                    return new { Status = false, Errors = errors };
                }
                contact = Mapper.Map<Contact>(model);
                context.SaveChanges();
                return new { success = true };
            }
            else
            {
                var errors = ModelState.GetModelErrors();
                return new
                {
                    Status = false,
                    Errors = errors
                };
            }
        }

        [HttpPost]
        [Route("api/contacts/delete")]
        public dynamic Delete(Guid Guid, Guid UserGuid)
        {
            var contact = context.Contacts.Where(c => c.UserGuid == UserGuid && c.GUID == Guid && c.ActiveContact).FirstOrDefault();
            if (contact != null)
            {
                contact.ActiveContact = false;
                context.SaveChanges();
                return new { Statues = true, Message = "Contact deleted" };
            }
            return new { Statues = false, Message = "Contact not found" };
        }
    }
}