﻿using ContactsAppWeb.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PagedList;
using PagedList.Mvc;
using ContactsAppWebAPI.ViewModels;

namespace ContactsAppWebAPI.Controllers
{
    public class GroupsController : ApiController
    {
        ContactsAppContext context;

        public GroupsController()
        {
            context = new ContactsAppContext();
        }

        [HttpGet]
        [Route("api/groups/getAll")]
        public IEnumerable<Group> GetAll(string pageNum)
        {
            int pageNumber = 1;
            if (!String.IsNullOrEmpty(pageNum))
                Int32.TryParse(pageNum, out pageNumber);
            int pageSize = 3;

            IQueryable<Group> groups = context.Groups.Include("SubGroups").OrderBy(c => c.GroupName);
            IPagedList<Group> pagedGroups = groups.ToPagedList(pageNumber, pageSize);

            return pagedGroups.ToList();
        }

        [HttpGet]
        [Route("api/groups/getGroupDetail/{id}")]
        public Group GetGroupDetail(int id)
        {
            var group = context.Groups.Include("Members").Where(c => c.ID == id).FirstOrDefault();
            return group;
        }

        [HttpGet]
        [Route("api/groups/getSubGroupDetail/{id}")]
        public SubGroup GetSubGroupDetail(int id)
        {
            var subGroup = context.SubGroups.Include("SubGroupMembers").Where(c => c.ID == id).FirstOrDefault();
            return subGroup;
        }

        [HttpPost]
        [Route("api/groups/addGroup/{groupName}")]
        public dynamic AddGroup(string groupName)
        {
            if (!String.IsNullOrEmpty(groupName))
                return new { Status = false, Error = "Enter group name" };

            var group = context.Groups.Where(c => c.GroupName == groupName).FirstOrDefault();
            if (group == null)
            {
                Group newGroup = new Group { GroupName = groupName };
                context.Groups.Add(newGroup);
                context.SaveChanges();
                return new { Status = true };
            }
            return new { Status = false, Error = "A group with same name already exists" };
        }

        [HttpPost]
        [Route("api/groups/addSubGroup/{subGroupName}/{groupId}")]
        public dynamic AddSubGroup(string subGroupName, int groupId)
        {
            if (!String.IsNullOrEmpty(subGroupName))
                return new { Status = false, Error = "Enter subgroup name" };

            var subGroup = context.SubGroups.Where(c => c.SubGroupName == subGroupName).FirstOrDefault();
            if (subGroup == null)
            {
                SubGroup newSubGroup = new SubGroup { SubGroupName = subGroupName, GroupID = groupId };
                context.SubGroups.Add(newSubGroup);
                context.SaveChanges();
                return new { Status = true };
            }
            return new { Status = false, Error = "A subgroup with same name already exists" };
        }

        [HttpPost]
        [Route("api/groups/editGroup/{id}/{groupName}")]
        public dynamic EditGroup(int id, string groupName)
        {
            //group must exitst
            //New name should not conflict with the existing one
            if (ModelState.IsValid)
            {
                var group = context.Groups.Find(id);
                if (group != null)
                {
                    var checkGroup = context.Groups.Where(c => c.GroupName == groupName);
                    var testGroup = checkGroup.Any(c => c.ID != id);
                    if (testGroup)
                    {
                        ModelState.AddModelError("GroupName", "A group with same name already exists");
                        return null;
                    }
                    else
                    {
                        group.GroupName = groupName;
                        context.SaveChanges();
                    }
                }
                return null;
            }
            else
            {
                var errors = ModelState.GetModelErrors();
                return errors;
            }
        }

        [HttpPost]
        [Route("api/groups/editSubGroup")]
        public dynamic EditSubGroup(SubGroup model)
        {
            //Subgroup must exitst
            //New name should not conflict with the existing one in its GROUP
            if (ModelState.IsValid)
            {
                var subGroup = context.SubGroups.Where(c => c.ID == model.ID && c.GroupID == model.GroupID).FirstOrDefault();
                if (subGroup != null)
                {
                    var checkGroup = context.SubGroups.Where(c => c.SubGroupName == model.SubGroupName);
                    var testGroup = checkGroup.Any(c => c.ID != model.ID && c.GroupID != model.GroupID);
                    if (testGroup)
                    {
                        ModelState.AddModelError("SubGroupName", "A subgroup with same name already exists");
                        return null;
                    }
                    else
                    {
                        subGroup.SubGroupName = model.SubGroupName;
                        subGroup.GroupID = model.GroupID;
                        context.SaveChanges();
                        return new { Status = true, Message = "" };
                    }
                }
                else
                {
                    ModelState.AddModelError("SubGroupName", "Subgroup do not exists");
                    return null;
                }
            }
            else
            {
                var errors = ModelState.GetModelErrors();
                return errors;
            }
        }

        [HttpGet]
        [Route("api/groups/getMembers/{id}")]
        public IEnumerable<GroupMember> GetMembersOfGroup(int id)
        {
            var group = context.Groups.Include("GroupMembers").Where(c => c.ID == id).FirstOrDefault();
            if (group != null)
            {
                var members = group.GroupMembers.Where(c => c.Approve).ToList();
                return members;
            }
            return null;
        }

        [HttpGet]
        [Route("api/subGroups/getMembers/{id}")]
        public IEnumerable<SubGroupMember> GetMembersOfSubGroup(int id)
        {
            var group = context.SubGroups.Include("SubGroupMembers").Where(c => c.ID == id).FirstOrDefault();
            if (group != null)
            {
                var members = group.SubGroupMembers.Where(c => c.Approve);
                return group.SubGroupMembers.ToList();
            }
            return null;
        }

        [HttpPost]
        [Route("api/groups/join")]
        public dynamic JoinGroup(int groupId, Guid UserGuid)
        {
            var user = context.Users.Where(c => c.GUID == UserGuid).FirstOrDefault();
            if (user != null)
            {
                //Should not have already pending request
                //Should not be a member already
                var alreadyMember = context.GroupMembers.Where(c => c.UserGuid == UserGuid && c.GroupID == groupId).FirstOrDefault();
                if (alreadyMember != null)
                {
                    if (alreadyMember.Approve)
                        return new { Status = false, Message = "Already a member of this group" };
                    else
                        return new { Status = false, Message = "Membership request already sent" };
                }

                GroupMember member = new GroupMember
                {
                    Approve = false,
                    BUID = user.BUID,
                    BusinessEmailAddress = user.Email,
                    //BusinessPhone = 
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    GroupID = groupId,
                    //Strengths = 
                    //Title = 
                    UserGuid = UserGuid
                };
                context.GroupMembers.Add(member);
                context.SaveChanges();
                return new { Status = true };
            }
            return new { Status = false, Message = "Invalid user" };
        }

        [HttpPost]
        [Route("api/subGroups/join")]
        public dynamic JoinSubGroup(int subGroupId, Guid UserGuid)
        {
            var user = context.Users.Where(c => c.GUID == UserGuid).FirstOrDefault();
            if (user != null)
            {
                //Should not have already pending request
                //Should not be a member already
                var alreadyMember = context.SubGroupMembers.Where(c => c.UserGuid == UserGuid && c.SubGroupID == subGroupId).FirstOrDefault();
                if (alreadyMember != null)
                {
                    if (alreadyMember.Approve)
                        return new { Status = false, Message = "Already a member of this subgroup" };
                    else
                        return new { Status = false, Message = "Membership request already sent" };
                }

                SubGroupMember member = new SubGroupMember
                {
                    Approve = false,
                    BUID = user.BUID,
                    BusinessEmailAddress = user.Email,
                    //BusinessPhone = 
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    SubGroupID = subGroupId,
                    //Strengths = 
                    //Title = 
                    UserGuid = UserGuid
                };
                context.SubGroupMembers.Add(member);
                context.SaveChanges();
                return new { Status = true };
            }
            return new { Status = false, Message = "Invalid user" };
        }

        public dynamic GetGroupRequests(string pageNum)
        {
            int pageNubmer = 1;
            int pageSize = 20;

            IQueryable<MembershipRequest> request =
                context.GroupMembers.Where(c => !c.Approve).Select
                (c => new MembershipRequest
                {
                    Id = c.ID,
                    IsGroup = true,
                    GroupSubGroupId = c.GroupID,
                    UserGuid = c.UserGuid
                });

            IPagedList<MembershipRequest> pagedRequests = request.ToPagedList(pageNubmer, pageSize);
            return pagedRequests.ToList();
        }

        public dynamic GetSubGroupRequests(string pageNum)
        {
            int pageNubmer = 1;
            int pageSize = 20;

            IQueryable<MembershipRequest> request =
                context.SubGroupMembers.Where(c => !c.Approve).Select
                (c => new MembershipRequest
                {
                    Id = c.ID,
                    IsGroup = true,
                    GroupSubGroupId = c.SubGroupID,
                    UserGuid = c.UserGuid
                });

            IPagedList<MembershipRequest> pagedRequests = request.ToPagedList(pageNubmer, pageSize);
            return pagedRequests.ToList();
        }

        [HttpPost]
        [Route("api/groups/approve")]
        public dynamic ApproveGroupMembership(int id)
        {
            var temp = context.GroupMembers.Find(id);
            if (temp != null)
            {
                if (!temp.Approve)
                {
                    temp.Approve = true;
                    context.SaveChanges();
                }
                else
                {
                    return new { Status = false, Message = "Already a member of this group" };
                }
            }
            return new { Status = false, Message = "Membership request not sent by the user" };
        }

        [HttpPost]
        [Route("api/subGroups/approve")]
        public dynamic ApproveSubGroupMembership(int id)
        {
            var temp = context.SubGroupMembers.Find(id);
            if (temp != null)
            {
                if (!temp.Approve)
                {
                    temp.Approve = true;
                    context.SaveChanges();
                    return new { Status = true, Message = "Membership request approved" };
                }
                else
                {
                    return new { Status = false, Message = "Already a member of this subgroup" };
                }
            }
            return new { Status = false, Message = "Membership request not sent by the user" };
        }
    }
}