﻿using ContactsAppWeb.Core.Models;
using ContactsAppWebAPI.ViewModels;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ContactsAppWebAPI.Controllers
{
    public class DirectoryController : ApiController
    {
        ContactsAppContext context;

        public DirectoryController()
        {
            context = new ContactsAppContext();
        }

        [HttpGet]
        [Route("api/directory/getAll")]
        public IEnumerable<User> GetAll(string pageNum)
        {
            int pageNumber = 1;
            int pageSize = 20;
            if (!String.IsNullOrEmpty(pageNum))
                Int32.TryParse(pageNum, out pageNumber);

            IQueryable<User> users = context.Users.Include("Provider").Include("Entity").Include("BU").Select(c => c);
            IPagedList<User> pagedUsers = users.ToPagedList(pageNumber, pageSize);
            return pagedUsers.ToList();
        }

        [HttpPost]
        [Route("api/directory/get")]
        public User GetDetail(Guid Guid)
        {
            var user = context.Users.Where(c => c.GUID == Guid).FirstOrDefault();
            return user;
        }
    }
}