﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.ModelBinding;
using ContactsAppWeb.Core;
using ContactsAppWeb.Core.ApiResponses;
using ApiResponses = ContactsAppWeb.Core.ApiResponses;

namespace ContactsAppWebAPI
{
    public static class ModelStateExtensions
    {
        public static IEnumerable<ApiResponses.ModelError> GetModelErrors(this ModelStateDictionary ModelState)
        {
            var modelStateErrors = ModelState.Keys.Where(key => ModelState[key].Errors.Any()).Select(key => key);
            var errors = modelStateErrors.Select(c => new ApiResponses.ModelError { Key = c, Error = ModelState[c].Errors.Select(d => d.ErrorMessage).FirstOrDefault() }).ToList();
            return errors;
        }
    }
}